from flask import Flask, session, render_template, url_for, request
from werkzeug.utils import redirect

app = Flask(__name__)

app.secret_key = b'\x12\x1eM\xa2;\xf8\xc1\x96W\xed\xe9-W\x136\x80'


@app.route('/')
def inicio():
    if 'username' in session:
        return f"El usuario {session['username']} ya ha hecho login"
    else:
        return 'No ha hecho login'

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        #Omitimos validacion de usuario y password
        #agregamos el usuario a la sesion
        session['username'] = request.form['username']
        return redirect(url_for('inicio'))
    return render_template('login.html')


@app.route('/logout')
def logout():
    session.pop('username')
    return redirect(url_for('inicio'))